package com.zuitt.wdc044.controllers;

import com.zuitt.wdc044.config.JwtToken;
import com.zuitt.wdc044.exceptions.UserException;
import com.zuitt.wdc044.models.User;
import com.zuitt.wdc044.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Optional;

@RestController
@CrossOrigin
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    private JwtToken jwtToken;


    // route for user registration
    @RequestMapping(value = "/users/register", method = RequestMethod.POST)
    public ResponseEntity<Object> register(@RequestBody Map<String, String> body) throws UserException {
        String username = body.get("username");

        if(!userService.findByUsername(username).isEmpty()) {
            throw new UserException("Username already exists.");
        } else {
            String password = body.get("password");
            String encodedPassword = new BCryptPasswordEncoder().encode(password);
            User newUser = new User(username, encodedPassword);
            userService.createUser(newUser);
            return new ResponseEntity<>("User registered successfully", HttpStatus.CREATED);
        }

    }

    @GetMapping("/userProfile")
    public ResponseEntity<Object> getUser(@RequestHeader(value = "Authorization") String stringToken) {
        Optional<User> optionalUser = userService.getUser(stringToken, jwtToken);

        return new ResponseEntity<>(optionalUser, HttpStatus.OK);
    }

    /*
    public ResponseEntity<Object> getUser(@RequestHeader(value = "Authorization") String stringToken) {
        return new ResponseEntity<>(userService.getUser(stringToken), HttpStatus.OK);
    }
    */

    @GetMapping("/myPosts")
    public ResponseEntity<Object> getUserPosts(@RequestHeader (value = "Authorization") String stringToken) {
        return new ResponseEntity<>(userService.getUserPosts(stringToken), HttpStatus.OK);
    }

}
