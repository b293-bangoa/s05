package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.Post;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Optional;

public interface PostService {

    void createPost(String stringToken, Post post);

    Iterable<Post> getPosts();

    Optional<Post> getPostById(@PathVariable Long postId);

    ResponseEntity updatePost(Long id, String stringToken, Post post);

    ResponseEntity deletePost(Long id, String stringToken);
}
