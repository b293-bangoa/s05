package com.zuitt.wdc044.services;

import com.zuitt.wdc044.config.JwtToken;
import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.models.User;
import com.zuitt.wdc044.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.Objects;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    JwtToken jwtToken;

    public void createUser(User user){
        userRepository.save(user);
    }

    public Optional<User> findByUsername(String username) {
        return Optional.ofNullable(userRepository.findByUsername(username));
    }

    public Optional<User> getUser(String stringToken, JwtToken jwtToken) {
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);
        User user = userRepository.findByUsername(authenticatedUser);
        user.setPassword("");

        return Optional.ofNullable(user);
    }

    /*
    public Object getUser(String stringToken) {
        User user = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        user.setPassword("");
        return user;
    }
    */

    public Object getUserPosts(String stringToken) {
        User user = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));

        return user.getPosts();
    }

}
