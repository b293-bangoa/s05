package com.zuitt.wdc044.services;

import com.zuitt.wdc044.config.JwtToken;
import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.models.User;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.Objects;
import java.util.Optional;

public interface UserService {

    void  createUser(User user);

    Optional<User> findByUsername(String username);

    Optional<User> getUser(@RequestHeader(value = "Authorization") String stringToken, JwtToken jwtToken);

    /*
    Object getUser (String stringToken);
    */

    Object getUserPosts(String stringToken);

}
